# Medusa Haptic Motor

![Medusa Haptic Motor](https://shop.edwinrobotics.com/4152-thickbox_default/medusa-haptic-motor.jpg "Medusa Haptic Motor")

[Medusa Haptic Motor](https://shop.edwinrobotics.com/modules/1239-medusa-haptic-motor.html)


The Medusa Haptic Motor works great as an inaudible source of indication, place the motor directly in contact with your skin to get notifications that only the wearer will know. The Medusa Haptic Motor has a wide operating voltage of 3-12V making it compatible with 3.3 & 5V systems.


Repository Contents
-------------------
* **/Design** - Eagle Design Files (.brd,.sch)
* **/Panel** - Panel file used for production

License Information
-------------------
The hardware is released under [Creative Commons ShareAlike 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/).

Distributed as-is; no warranty is given.